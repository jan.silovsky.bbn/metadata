import typing

import pandas  # type: ignore

from .. import metadata as metadata_module

__all__ = ('DataFrame', 'SparseDataFrame')


class DataFrame(pandas.DataFrame):
    """
    Extended `pandas.DataFrame` with the `metadata` attribute.

    `pandas.DataFrame` **does** allow directly attaching `metadata` attribute to an instance.
    """

    metadata: metadata_module.DataMetadata = None


class SparseDataFrame(pandas.SparseDataFrame):
    """
    Extended `pandas.SparseDataFrame` with the `metadata` attribute.

    `pandas.SparseDataFrame` **does** allow directly attaching `metadata` attribute to an instance.
    """

    metadata: metadata_module.DataMetadata = None


typing.Sequence.register(pandas.DataFrame)  # type: ignore
typing.Sequence.register(pandas.SparseDataFrame)  # type: ignore
