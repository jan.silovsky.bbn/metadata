import typing

from .. import metadata as metadata_module

__all__ = ('List',)

# We should really see this list as immutable and this is why covariance is a reasonable choice here.
# See: PEP 484 and 483 for more details about immutability and covariance
# See: https://github.com/Stewori/pytypes/issues/21
# See: https://gitlab.com/datadrivendiscovery/metadata/issues/1
T = typing.TypeVar('T', covariant=True)


class List(typing.List[T]):
    """
    Extended Python standard `typing.List` with the `metadata` attribute.

    You have to create a subclass of this generic type with specified element type before
    you can instantiate lists.

    One can use various base and container types as its elements.
    """

    metadata: metadata_module.DataMetadata = None
